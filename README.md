# README #

Тестовое задание
Необходимо создать Android приложение - чат для обмена сообщениями
Для упрощения задания предусмотреть наличие всего одной комнаты-чата для общения, к которой подключаются все пользователи, запускающие приложение
Виды сообщений:
- текстовые
- фото
- аудио
Также необходимо оповещать пользователя о новом сообщении push-уведомлением в случае, если окно чата свернуто.
Для реализации необходимо использовать Firebase (Realtime Database - для чата, Cloud Messaging - для push-уведомлений)