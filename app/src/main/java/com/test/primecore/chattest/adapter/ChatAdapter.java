package com.test.primecore.chattest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.primecore.chattest.util.FirebaseHandler;
import com.test.primecore.chattest.R;
import com.test.primecore.chattest.interfaces.ImageCallback;
import com.test.primecore.chattest.models.Chat;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<Chat> chatArrayList;
    private ImageCallback imageCallback;

    private static final int RIGHT_MSG = 0;
    private static final int LEFT_MSG = 1;


    public ChatAdapter(Context context, ArrayList<Chat> chatArrayList, ImageCallback imageCallback) {
        this.context = context;
        this.chatArrayList = chatArrayList;
        this.imageCallback = imageCallback;
    }

    public class HolderLeft extends RecyclerView.ViewHolder {

        private TextView name, timestamp, message;
        private ImageView photo;

        public HolderLeft(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
            message = (TextView) itemView.findViewById(R.id.message);
            photo = (ImageView) itemView.findViewById(R.id.media_attachments);

            photo.setOnClickListener(v -> {
                imageCallback.getImage(chatArrayList.get(getAdapterPosition()).getAttachedMedia());
            });
        }


    }

    public class HolderRight extends RecyclerView.ViewHolder {
        private TextView name, timestamp, message;
        private ImageView photo;

        public HolderRight(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
            message = (TextView) itemView.findViewById(R.id.message);
            photo = (ImageView) itemView.findViewById(R.id.media_attachments);

            photo.setOnClickListener(v -> {
                imageCallback.getImage(chatArrayList.get(getAdapterPosition()).getAttachedMedia());
            });

        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case LEFT_MSG:
                View leftView = inflater.inflate(R.layout.item_left, parent, false);
                viewHolder = new HolderLeft(leftView);
                break;
            case RIGHT_MSG:
                View rightView = inflater.inflate(R.layout.item_right, parent, false);
                viewHolder = new HolderRight(rightView);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case LEFT_MSG:
                HolderLeft holderLeft = (HolderLeft) holder;
                holderLeft.message.setText(chatArrayList.get(position).getMessage());
                holderLeft.name.setText(chatArrayList.get(position).getName());
                holderLeft.timestamp.setText(convertTimeStamp(chatArrayList.get(position).getTimeStamp()));
                if (chatArrayList.get(position).getAttachedMedia() != null) {
                    holderLeft.photo.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(chatArrayList.get(position).getAttachedMedia()).placeholder(R.drawable.ic_music_note_red_400_36dp).into(holderLeft.photo);
                }
                break;
            case RIGHT_MSG:
                HolderRight holderRight = (HolderRight) holder;
                holderRight.message.setText(chatArrayList.get(position).getMessage());
                holderRight.name.setText(chatArrayList.get(position).getName());
                holderRight.timestamp.setText(convertTimeStamp(chatArrayList.get(position).getTimeStamp()));
                if (chatArrayList.get(position).getAttachedMedia() != null) {
                    holderRight.photo.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(chatArrayList.get(position).getAttachedMedia()).placeholder(R.drawable.ic_music_note_red_400_36dp).into(holderRight.photo);
                }

                break;
        }

    }


    @Override
    public int getItemCount() {
        return chatArrayList.size();
    }


    @Override
    public int getItemViewType(int position) {
        Chat chat = chatArrayList.get(position);
        if (chat.getUID().equals(FirebaseHandler.firebaseUser.getUid()))
            return RIGHT_MSG;
        else
            return LEFT_MSG;
    }


    private CharSequence convertTimeStamp(String timestamp) {
        return DateUtils.getRelativeTimeSpanString(Long.parseLong(timestamp), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }
}
