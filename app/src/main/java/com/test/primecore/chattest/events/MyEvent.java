package com.test.primecore.chattest.events;

import android.net.Uri;

import com.test.primecore.chattest.models.Chat;

import java.util.ArrayList;

public class MyEvent {


    public static class UploadEvent {
        public boolean isSuccessful;
        public Uri downloadURL;

        public UploadEvent(boolean isSuccessful, Uri downloadURL) {
            this.isSuccessful = isSuccessful;
            this.downloadURL = downloadURL;
        }

        public Uri getDownloadURL() {
            return downloadURL;
        }

        public boolean isSuccessful() {
            return isSuccessful;
        }
    }



    public static class GetChats {
        public ArrayList<Chat> chatArrayList;

        public GetChats(ArrayList<Chat> chatArrayList){
            this.chatArrayList = chatArrayList;
        }

        public ArrayList<Chat> getChatArrayList() {
            return chatArrayList;
        }
    }
}
