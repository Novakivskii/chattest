package com.test.primecore.chattest.view.fragments;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.test.primecore.chattest.R;
import com.squareup.picasso.Picasso;

@SuppressLint("ValidFragment")
public class PreviewFragment extends DialogFragment {


    private String url;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AlertDialogStyle);

    }


    @SuppressLint("ValidFragment")
    public PreviewFragment(String url) {
        this.url = url;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.show_image, container);
        initUI(view);
        return view;
    }


    private void initUI(View view) {
        ImageView image = (ImageView) view.findViewById(R.id.show_image);
        Picasso.with(getActivity()).load(url).into(image);


    }
}
