package com.test.primecore.chattest.util;

import android.net.Uri;
import android.text.format.DateFormat;
import android.util.Log;

import com.test.primecore.chattest.events.MyEvent;
import com.test.primecore.chattest.models.Chat;
import com.test.primecore.chattest.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.content.ContentValues.TAG;

public class FirebaseHandler {

    private static FirebaseHandler firebaseHandler;

    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl(Constants.BASE_URL);
    private DatabaseReference messageReference = databaseReference.child(Constants.MESSAGES);
    private DatabaseReference userReference = databaseReference.child(Constants.USERS);
    public static FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private ArrayList<Chat> chatArrayList = new ArrayList<>();

    public static FirebaseHandler getFirebaseHandler() {
        if (firebaseHandler != null)
            return firebaseHandler;
        else
            return firebaseHandler = new FirebaseHandler();
    }

    public void saveUser(User user) {
        DatabaseReference individualUser = userReference.child(user.getId());
        individualUser.setValue(user);
    }

    public void sendMessage(Chat message) {
        DatabaseReference chats = messageReference.push();
        chats.setValue(message);
    }

    public void getChatData() {

        messageReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, dataSnapshot.getValue() + " -- ADDED");
                Chat chat = dataSnapshot.getValue(Chat.class);
                chatArrayList.add(chat);
                EventBus.getDefault().post(new MyEvent.GetChats(chatArrayList));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void sendFileFirebase(StorageReference storageReference, final Uri file, int type) {
        if (storageReference != null) {
            final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
            if (type == 1) {
                StorageReference imageGalleryRef = storageReference.child(name + "_audio");
                UploadTask uploadTask = imageGalleryRef.putFile(file);
                uploadTask.addOnFailureListener(e -> Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage()))
                        .addOnSuccessListener(taskSnapshot -> {
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            Chat chat = new Chat();
                            chat.setName(firebaseUser.getDisplayName());
                            chat.setAttachedMedia(downloadUrl.toString());
                            chat.setTimeStamp(getTimeStamp());
                            chat.setProfileURL(firebaseUser.getPhotoUrl().toString());
                            chat.setUID(firebaseUser.getUid());
                            sendMessage(chat);
                            EventBus.getDefault().post(new MyEvent.UploadEvent(true, downloadUrl));
                        });
            } else if (type == 0) {
                StorageReference imageGalleryRef = storageReference.child(name + "_gallery");
                UploadTask uploadTask = imageGalleryRef.putFile(file);
                uploadTask.addOnFailureListener(e -> Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage()))
                        .addOnSuccessListener(taskSnapshot -> {
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            Chat chat = new Chat();
                            chat.setName(firebaseUser.getDisplayName());
                            chat.setAttachedMedia(downloadUrl.toString());
                            chat.setTimeStamp(getTimeStamp());
                            chat.setProfileURL(firebaseUser.getPhotoUrl().toString());
                            chat.setUID(firebaseUser.getUid());
                            sendMessage(chat);
                            EventBus.getDefault().post(new MyEvent.UploadEvent(true, downloadUrl));
                        });
            }

        }

    }

    public String getTimeStamp() {
        return Calendar.getInstance().getTime().getTime() + "";
    }
}
