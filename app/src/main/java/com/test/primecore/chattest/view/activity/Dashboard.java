package com.test.primecore.chattest.view.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.test.primecore.chattest.util.Constants;
import com.test.primecore.chattest.util.FirebaseHandler;
import com.test.primecore.chattest.R;
import com.test.primecore.chattest.adapter.ChatAdapter;
import com.test.primecore.chattest.events.MyEvent;
import com.test.primecore.chattest.interfaces.ImageCallback;
import com.test.primecore.chattest.models.Chat;
import com.test.primecore.chattest.view.fragments.PreviewFragment;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.tbruyelle.rxpermissions.RxPermissions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static com.test.primecore.chattest.util.FirebaseHandler.firebaseUser;

public class Dashboard extends AppCompatActivity implements View.OnClickListener, ImageCallback {

    private static final int FROM_GALLERY = 2;
    private static final int FROM_AUDIO = 3;

    private static final String TAG = Dashboard.class.getSimpleName();

    private TextView chatTitle;
    private File filePathCameraImage;
    private ImageView addAttachment, sendMessage;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private ProgressBar uploadProgress;
    private EditText edtMessage;
    private CoordinatorLayout coordinatorLayout;
    private RecyclerView recyclerView;
    private ChatAdapter adapter;
    private ArrayList<Chat> chatArrayList = new ArrayList<>();
    private Set<Chat> chatSet = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initUI();
        getFirebaseData();
    }

    private void getFirebaseData() {
        FirebaseHandler.getFirebaseHandler().getChatData();
    }

    private void initUI() {

        chatTitle = (TextView) findViewById(R.id.chat_header);
        addAttachment = (ImageView) findViewById(R.id.add_attachment);
        sendMessage = (ImageView) findViewById(R.id.send_message);

        recyclerView = (RecyclerView) findViewById(R.id.rv_chat);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ChatAdapter(this, chatArrayList, this);
        recyclerView.setAdapter(adapter);

        uploadProgress = (ProgressBar) findViewById(R.id.upload_progress);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        edtMessage = (EditText) findViewById(R.id.edt_message);

        addAttachment.setOnClickListener(this);
        sendMessage.setOnClickListener(this);
    }

    public void addMediaAttachment() {
        final CharSequence[] items = {"Choose from Gallery", "Audio", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Media");
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Choose from Gallery")) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, FROM_GALLERY);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            } else if (items[item].equals("Audio")) {
                Intent pickAudioIntent = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickAudioIntent, FROM_AUDIO);

            }
        });
        builder.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_attachment:
                RxPermissions.getInstance(this)
                        .request(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribe(granted -> {
                            if (granted) {
                                addMediaAttachment();
                            } else {
                                Snackbar.make(coordinatorLayout, "Permission Denied", Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        });
                break;
            case R.id.send_message:
                if (edtMessage.length() > 0) {
                    Chat chat = new Chat();
                    chat.setProfileURL(firebaseUser.getPhotoUrl().toString());
                    chat.setTimeStamp(FirebaseHandler.getFirebaseHandler().getTimeStamp());
                    chat.setName(firebaseUser.getDisplayName());
                    chat.setMessage(edtMessage.getText().toString());
                    chat.setUID(firebaseUser.getUid());
                    FirebaseHandler.getFirebaseHandler().sendMessage(chat);
                    edtMessage.setText("");


                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        StorageReference storageRef = storage.getReferenceFromUrl(Constants.STORAGE_URL).child(Constants.STORAGE_PATH);

        if (requestCode == FROM_GALLERY) {
            if (resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
                    FirebaseHandler.getFirebaseHandler().sendFileFirebase(storageRef, selectedImageUri, 0);
                    toggleProgress(1);
                    showSnackBar();
                } else
                    Snackbar.make(coordinatorLayout, "NO URI - GALLERY ", Snackbar.LENGTH_LONG)
                            .show();
            }

        } else if (requestCode == FROM_AUDIO) {
            if (resultCode == RESULT_OK) {
                Uri selectedAudio = data.getData();
                if (selectedAudio != null) {
                    FirebaseHandler.getFirebaseHandler().sendFileFirebase(storageRef, selectedAudio, 1);
                    toggleProgress(1);
                    showSnackBar();
                } else {
                    Snackbar.make(coordinatorLayout, "NO URI - AUDIO", Snackbar.LENGTH_LONG)
                            .show();
                }

            }
        }
    }


    private void showSnackBar() {
        Snackbar.make(coordinatorLayout, "UPLOADING ...", Snackbar.LENGTH_LONG)
                .show();
    }

    @Subscribe
    public void uploadSuccessfully(MyEvent.UploadEvent uploadEvent) {
        if (uploadEvent.isSuccessful()) {
            Snackbar.make(coordinatorLayout, "Upload Complete", Snackbar.LENGTH_LONG)
                    .show();
            toggleProgress(0);
        }
    }


    @Subscribe
    public void updateUI(MyEvent.GetChats getChats) {
        chatArrayList.clear();
        if (getChats != null) {
            chatArrayList.addAll(getChats.getChatArrayList());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void toggleProgress(int state) {
        if (state == 0) {
            uploadProgress.setVisibility(View.GONE);
        } else if (state == 1) {
            uploadProgress.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void getImage(String url) {

        if (url.contains("_gallery")) {
            PreviewFragment previewFragment = new PreviewFragment(url);
            previewFragment.show(getFragmentManager(), previewFragment.getClass().getSimpleName());
        } else if (url.contains("_audio")) {
            Uri myUri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(myUri, "audio/*");
            startActivity(intent);

        }
    }
}
