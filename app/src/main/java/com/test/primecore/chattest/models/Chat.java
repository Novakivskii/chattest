package com.test.primecore.chattest.models;



public class Chat {

    private String name;
    private String message;
    private String attachedMedia;
    private String profileURL;
    private String timeStamp;
    private String UID;


    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public Chat(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAttachedMedia() {
        return attachedMedia;
    }

    public void setAttachedMedia(String attachedMedia) {
        this.attachedMedia = attachedMedia;
    }

    public String getProfileURL() {
        return profileURL;
    }

    public void setProfileURL(String profileURL) {
        this.profileURL = profileURL;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
