package com.test.primecore.chattest;

import android.app.Application;
import android.content.Context;

import com.google.firebase.database.FirebaseDatabase;

public class MyApplication extends Application {


    private static MyApplication instance;

    public static MyApplication getInstance() {
        if (instance != null)
            return instance;
        else
            return instance = new MyApplication();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

    }


    public static Context getContext() {
        return MyApplication.getInstance().getApplicationContext();
    }


}
