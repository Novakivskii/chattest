package com.test.primecore.chattest.util;
public class Constants {

    public static final String BASE_URL = "https://testchat-4c0a2.firebaseio.com/";
    public static final String USERS = "users";
    public static final String MESSAGES = "messages";
    public static final String STORAGE_URL = "gs://testchat-4c0a2.appspot.com";
    public static final String STORAGE_PATH = "storage";

}
